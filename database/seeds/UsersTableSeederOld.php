<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        // $users = array(
        //     array(
        //     'email' => 'jmac@confidentlaravel.com',
        //     'name' => 'Jason McCreary',
        //     'password' => Hash::make('mccreaja')
        // ));

        // DB::table('users')->insert($users);

        DB::table('users')->insert([
            'email' => 'jmac@confidentlaravel.com',
            'name' => 'Jason McCreary',
            'password' => Hash::make('mccreaja')
        ]);

        // DB::table('users')->insert([
        //     'email' => Str::random(10).'@gmail.com',
        //     'name' => Str::random(10),
        //     'password' => Hash::make('password')
        // ]);

        // User::factory()
        //     ->count(10)
        //     ->create();
    }
}
