<?php

namespace Database\Factories;

use App\Order;
use App\Coupon;
use App\Models\User;
use App\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    use HasFactory;
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //closure
            'user_id' => function(){
                return User::factory()->create()->id;
            },

            'product_id' => $this->faker->randomDigitNotNull,

            // 'product_id' => function () {
            //     return Product::factory()->starter()->create()->id;


            // },

            'stripe_id' => $this->faker->word,
            'coupon_id' => function(){
                return Coupon::factory()->create()->id;
            },
            'total' => $this->faker->randomFloat(2,1,100),
        ];
    }
}

