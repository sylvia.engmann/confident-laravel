<?php

namespace Database\Factories;

use App\Video;
use App\Lesson;
use Illuminate\Database\Eloquent\Factories\Factory;

class VideoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Video::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //words returns an array, so to get the actual text second parameter in words() is set to true
            'title' => $this->faker->words(3,true),
            'heading' => $this->faker->sentence,
            'summary' => $this->faker->text,
            'vimeo_id' => $this->faker->md5,
            'ordinal' => $this->faker->randomNumber(4, true),
            //'lesson_id' => $this->faker->randomNumber(),
            'lesson_id' => function(){
                return Lesson::factory()->create()->id;
 
             }
 
        ];
    }
}
