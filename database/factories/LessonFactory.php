<?php

namespace Database\Factories;

use App\Lesson;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lesson::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' =>$this->faker->name,
            'ordinal' => $this->faker->randomNumber(),
            //'product_id' => $this->faker->randomNumber(),
            'product_id' => null,

        ];
    }
}
