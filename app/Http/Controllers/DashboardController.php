<?php

namespace App\Http\Controllers;

use App\Video;
use App\Lesson;

class DashboardController extends Controller
{
    public function index(\Illuminate\Http\Request $request)
    {
        $user = $request->user();
        if (!$user->last_viewed_video_id) {
            $user->last_viewed_video_id = 1;
            $user->save();
        }
        //check what user is logged in
        //dd($user);


        $now_playing = Video::find($user->last_viewed_video_id);
        //check what is playing
        //dd($now_playing);

        //included this line
        $lessons = Lesson::course();
        //check if the lessons exist
        //dd($lessons);

        //return view('videos.show', compact('now_playing'));
        // return view('videos.show', compact('now_playing',[
        //     'lessons' => Lesson::course()],
        // ));

        // return view('videos.show', compact('now_playing',[
        //     'lessons' => $lessons],
        // ));

        return view('videos.show', [
            'now_playing'=> $now_playing,
            'lessons' => $lessons],
        );
    }
}
