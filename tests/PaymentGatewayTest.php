<?php

namespace Tests;

use App\Order;
use App\Services\PaymentGateway;

class PaymentGatewayTest extends \PHPUnit\Framework\TestCase
{
    public function testCharge(){

        $subject = new PaymentGateway();

        $token = $this->createTestToken();
        $order = new Order();

        $actual = $subject->charge($token, $order);

        $charge = $this->getStripeCharge($actual);

        $this->assertEqual($charge->total = $order->total);
    }

    private function createTestToken()
    {
        # code...
    }

    private function getStripeCharge(string $actual)
    {

    }
}