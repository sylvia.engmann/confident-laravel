<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use App\Video;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tests\TestCase;

class DashboardControllerTest extends TestCase
{
    #use RefreshDatabase;
    use HasFactory;

    /**
     * @test
     * */
    public function it_retrieves_the_last_watched_video()
    {
        $this->withoutExceptionHandling(); 

        $video = Video::factory()->create();

        $user = User::factory()->create([
            'last_viewed_video_id' => $video->id
        ]);

        $response = $this->actingAs($user)->get('/dashboard');
        #$response = $this->actingAs($user)->get(route('videos.show'));

        $response->assertStatus(200);
        $response->assertViewIs('videos.show');
        $response->assertViewHas('now_playing', $video);
    }

    /**
     * @test
     */
    // public function it_defaults_last_video_for_a_new_user()
    // {
    //     $this->withoutExceptionHandling(); 

    //     $video = Video::factory()->create();
    //     $user = User::factory()->create();

    //     $response = $this->actingAs($user)->get('/dashboard');

        
    //     $response->assertStatus(200);
    //     $response->assertViewIs('videos.show');
    //     $response->assertViewHas('now_playing', $video);

    //     //arbitary data
    //     // $this->assertDatabaseHas('users', [
    //     //     'id'=> $user->id,
    //     //     'last_viewed_video_id'=> $video->id,
    //     // ]);

    //     //already existing record
    //     $user->refresh();
    //     $this->assertEquals($video->id, $user->last_viewed_video_id);
    //  }

    
    
}
