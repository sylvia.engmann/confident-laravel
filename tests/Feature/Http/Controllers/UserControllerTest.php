<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Controllers\UsersController;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use JMac\Testing\Traits\HttpTestAssertions;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    //use HttpTestAssertions;
    /**
     * @test
     */
    public function updates_saves_data_and_redirects_to_dashboard()
    {
        #$this->withoutExceptionHandling(); 

        $user = User::factory()->create();
        
        $name = $this->faker->name;
        $password = $this->faker->password(8);

        $response = $this->actingAs($user)->put('/users',[
            'name' => $name,
            'password' => $password,
            'password_confirmation' => $password,

        ]);

        $response->assertRedirect('/dashboard');

        $user->refresh();
        $this->assertEquals($name, $user->name);
        $this->assertTrue(Hash::check($password, $user->password));

    }

    // /**
    //  * @test
    //  */
    // public function update_uses_validation()
    // {
    //     $this->assertActionUsesFormRequest(
    //         UsersController::class,
    //         'update',
    //         UserUpdateRequest::class

    //     );
    // }

    //the instructor's test for validation on the whole object failed
    //so its commented out

    /**
     * @test
     */
    public function update_fails_for_invalid_name()
    {
        $user = User::factory()->create();

        $password = $this->faker->password(8);

        $response = $this->from(route('user.edit'))->actingAs($user)->put('/users',[
            'name' => null,
            'password' => $password,
            'password_confirmation' => $password,
        ]);

        $response->assertRedirect(route('user.edit'));
        $response->assertSessionHasErrors('name');

    }

    /**
     * @test
     */
    public function update_fails_for_invalid_password()
    {
        $user = User::factory()->create();

        $name = $this->faker->name;

        $response = $this->from(route('user.edit'))->actingAs($user)->put('/users',[
            'name' => $name,
            'password' => null,
            'password_confirmation' => null,
        ]);

        $response->assertRedirect(route('user.edit'));
        $response->assertSessionHasErrors('password');
    }
}
