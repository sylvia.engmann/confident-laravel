<?php

namespace Tests\Feature\Http\Controllers;

use App\Order;
use App\Product;
use App\Models\User;
use App\Services\PaymentGateway;
use App\Exceptions\PaymentGatewayChargeException;
use App\Mail\OrderConfirmation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
//use Stripe\Error\Card;
use Stripe\Card as Card;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;


class OrderControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     */
    public function store_charges_for_order_and_creates_account()
    {

        $this->withoutExceptionHandling();

        $product = Product::factory()->create();
        $token = $this->faker->md5;
        $email = $this->faker->safeEmail;
        $charge_id = $this->faker->md5;

        $paymentGateway = $this->mock(PaymentGateway::class);
        $paymentGateway->shouldReceive('charge')
        ->with($token, \Mockery::on(function($argument) use($product){
            return $argument->$product == $product->id
            && $argument-> total == $product->price;
        }))
        ->andReturn($charge_id);

        $event = Event::fake();   
        $mail = Mail::fake(); 


        $response = $this->post(route('order.store'), [
            'product_id' => $product->id,
            'stripeToken' => $token,
            'stripeEmail' => $email
        ]);

        $response->assertRedirect('/users/edit');
        $users = User::where('email', $email)->get();

        $this->assertSame(1, $users->count());

        $user = $users->first();
        $this->assertAuthenticatedAs($user);

        $this->assertDatabaseHas('orders', [
            'product_id' => $product->id,
            'total' => $product->price,
            'user_id' => $user->id,
            'stripe_id' => $charge_id

        ]);

        $order = Order::where('stripe_id', $charge_id)->first();
        $event->assertDispatched('order.placed', function($event, $argument) use($order){
            return $argument->is($order);
    
           });

        $mail->assertSent(OrderConfirmation::class, function($mail) use($order,$user){
            return $mail->order->is($order)
            && $mail->hasTo($user->email);
        });
    }

    /**
     * @test
     */
    public function store_returns_error_view_when_charge_fails(){

        //$this->withoutExceptionHandling();


        $product = Product::factory()->create();
        $token = $this->faker->md5;

        $paymentGateway = $this->mock(PaymentGateway::class);
        $exception = new PaymentGatewayChargeException(
            'sad path order exception',
            ['error' => ['data' => 'passed to view']]
        );
        $paymentGateway->shouldReceive('charge')
        ->with($token, \Mockery::type(Order::class))
        ->andThrows($exception);

        $response = $this->post(route('order.store'), [
            'product_id' => $product->id,
            'stripeToken' => $token,
            'stripeEmail' => $this->faker->safeEmail
        ]);

        $response->assertOk();
        $response->assertViewIs('errors.generic');
        $response->assertViewHas('template', 'partials.errors.charge_failed');
        $response->assertViewHas('data', ['data' => 'passed to view']);
    }

    /**
     * 
     */
    public function store_applies_coupon_to_order(){
        $this->markTestIncomplete();
    }
}
