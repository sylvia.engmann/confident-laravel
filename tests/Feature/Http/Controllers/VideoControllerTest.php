<?php

namespace Tests\Feature\Http\Controllers;

use App\Lesson;
use App\Models\User;
use App\Video;
use App\Product;
use App\Order;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VideoControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function show_sets_last_video_and_display_view()
    {
        $this->withoutExceptionHandling();
        
        $user = User::factory()->create();
        $video = Video::factory()->create();
        $response = $this->actingAs($user)->get(route('videos.show', $video->id));

        //dd($response);
        $response->assertStatus(200);
        $response->assertViewIs('videos.show');
        $response->assertViewHas('now_playing', $video);

        $user->refresh();
        $this->assertEquals($video->id, $user->last_viewed_video_id);
    }

    /**
     */
    public function show_returns_403_when_user_does_not_have_access()
    {

        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        // $lesson = Lesson::factory()->create([
        //     'product_id' => Product::FULL
        // ]);

        // $video = Video::factory()->create([
        //     'lesson_id' => $lesson->id
        // ]);     
        
        $video = Video::factory()
        ->forLesson([ 'product_id' => Product::FULL])
         ->create();

        $order = Order::factory()->create([
            'user_id' => $user->id,
            'product_id' => Product::STARTER
        ]);

        $response = $this->actingAs($user)->get(route('videos.show', $video->id));

        $response->assertForbidden();
      
    }

}
