<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use App\Video;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;

use Mockery;
use Tests\TestCase;

class WatchesControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    /**
     * @test
     */
    public function store_returns_a_204()
    {

        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $video = Video::factory()->create();

    //     $mock = Mockery::mock();
    //     $mock->shouldReceive('info')->once()->with('video.watched', [$video->id]);

    //    // Event::swap($mock);
    //     Event::fake();

        $event = Event::fake();

        $response = $this->actingAs($user)->post(route('watches.store'), [
            'user_id' => $user->id,
            'video_id' => $video->id
        ]);


        $response->assertStatus(204);
    //     $event->assertDispatched('video.watched', function($event, $arguments) use($video){
    //     return $arguments == [$video -> id];
    // });

        $event->assertDispatched('video.watched', function($event, $arguments) use($video){
            $this->assertEquals([$video->id], $arguments, 'The arguments passed to the [' . $event. '] event were unexpected');

                return true;

        });



        $this->assertDatabaseHas('watches', [
            'user_id' => $user->id,
            'video_id' => $video->id
        ]);
    }
}
