<?php

namespace Tests\Feature\Http\Controllers;

use App\Coupon;
use App\Models\User;
use App\Order;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponControllerTest extends TestCase
{
    use RefreshDatabase;
    use HasFactory;
    /**
     * @test
     */
    public function it_stores_coupon_and_redirects()
    {
        //use a coupon in the database
        $coupon = Coupon::factory()->create();
        $response = $this->get('/promotions/'. $coupon->code);

        $response->assertRedirect('/#buy-now');
        $response->assertSessionHas('coupon_id', $coupon->id);
    }

    /**
     * @test
     */
    public function it_does_not_store_coupon_for_invalid_code()
    {
        //no coupon recreation necessary
        #$coupon = Coupon::factory()->create();

        $response = $this->get('/promotions/invalid-code');
        $response->assertRedirect('/#buy-now');
        #$response->assertSessionMissing('coupon_id', $coupon->id);
        $response->assertSessionMissing('coupon_id');

    }

    /**
     * @test
     */
    public function it_does_not_store_an_expired_coupon()
    {
        $coupon = Coupon::factory()->create([
            'expired_at' => now()
        ]);
        $response = $this->get('/promotions/'. $coupon->code);
        
        $response->assertRedirect('/#buy-now');
        $response->assertSessionMissing('coupon_id');

    }

    /**
     * @test
     */
    public function it_does_not_store_a_previously_used_coupon()
    {
        $user = User::factory()->create();

        $coupon = Coupon::factory()->create([
            'expired_at' => now() //had to add this for the coupon to expire, otherwise test fails

        ]);
        Order::factory()->create([
            'user_id' => $user->id,
            'coupon_id' => $coupon->id,
        ]);

        $response = $this->actingAs($user)->get('/promotions/'. $coupon->code);

        $response->assertRedirect('/#buy-now');
        $response->assertSessionMissing('coupon_id');

    }
}