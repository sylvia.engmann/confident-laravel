<?php

namespace Tests\Feature\Http\Controllers\Auth;

use App\Models\User;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;
    use HasFactory;
    #use WithoutMiddleware;

    /**
     *@test
     */
    public function login_redirects_to_dashboard()
    {
        $this->withoutExceptionHandling(); 

        $user = User::factory()->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertRedirect('dashboard');

        //assert user is logged in
        $this->assertAuthenticatedAs($user);

    }
}
