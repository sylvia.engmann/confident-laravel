<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * @test
     */
    public function testMocking()
    {
        $mock = Mockery::mock();

        $mock->shouldReceive('foo')->with('bar')->andReturn('baz');

        $this->assertEquals('baz', $mock->foo('bar'));

        $mock->shouldReceive('qux')->andReturnNull();
        $this->assertNull($mock->qux());
    }

    /**
     * @test
     */
    public function testSpying(){
        $spy = Mockery::spy();

        $this->assertNull($spy->qux());

        $spy->shouldHaveReceived('qux')->once();
    }
}
