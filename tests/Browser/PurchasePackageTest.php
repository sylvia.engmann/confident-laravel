<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PurchasePackageTest extends DuskTestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function it_can_purchase_the_starter_package_and_create_account()
    {
        $this->browse(function ($browser) {
            $browser->visit('/')
                    ->clickLink('Buy Now $89')
                    ->waitFor('iframe[name=stripe_checkout_app]')
                    ->withinFrame('iframe[name=stripe_checkout_app]', function($browser){
                       $browser->assertSee('Starter');
                       $browser->keys('input[placeholder="Email"]', $this->faker->safeEmail)
                           ->keys('input[placeholder="Card number"]', '4242424242424242')
                           ->keys('input[placeholder="MM / YY"]', '01'.now()->addYear()->format('y'))
                           ->keys('input[placeholder="CVC"]', '123')
                           ->press('button[type="submit"')
                           ->waitUntilMissing('iframe[name=stripe_checkout_app]');
                })
                ->waitForReload()
                ->assertPathIs('/users/edit');
        });
    }
}
