<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class DocumentationLinkTest extends DuskTestCase
{
    /**
     *
     * @test
     */
    public function links_to_the_laravel_documentation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/');
                    if($browser->seeLink('Documentation')){
                        $browser->clickLink('Documentation')
                                ->assertUrlIs('https://laravel.com/docs/8.x');
                    }
        });
    }
}
